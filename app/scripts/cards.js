console.log('Cargando Cards...');
const dataCards = [{
    "title": "Juega solo o con amigos",
    "url_image":"https://static.independent.co.uk/s3fs-public/thumbnails/image/2017/11/15/18/league-of-legends.jpg?w968h681",
    "desc":"No es necesario ser parte de un equipo, aunque no está de más un poco de ayuda.",
    "cta":"Show More",
    "link":"https://www.edsurge.com/news/2019-01-22-educators-share-how-video-games-can-help-kids-build-sel-skills"
},
{
    "title": "Tipos de Need for Speed",
    "url_image":"https://www.motor.es/fotos-noticias/2019/07/need-for-speed-heat-adelanto-201959561-1564487833_1.jpg",
    "desc":"Conoce todos los tipos de Need For Speed existentes.",
    "cta":"Show More",
    "link":"https://www.zonared.com/juegos/sagas/need-for-speed/"
},
{   "title": "Juegos de Accion",
    "url_image":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSOUJtkOO3j-N11ntrzVkyqEggT1BUkQekd0w&usqp=CAU",
    "desc":"Te Recomendamos los mejores juegos de acción en el mercado.",
    "cta":"Show More",
    "link":"https://as.com/meristation/juegos/top/videojuegos_accion/"
},
{
    "title": "Desde el Inicio",
    "url_image":"https://i.guim.co.uk/img/media/7c2ab1a3e60e445caf0a4d3de302591e830e8f7f/0_0_3800_2280/master/3800.jpg?width=465&quality=45&auto=format&fit=max&dpr=2&s=5ad89e6e0ed1bfe9939df81ddbe05d48",
    "desc":"Conoce un poco sobre los juegos mas populares.",
    "cta":"Show More",
    "link":"https://www.hobbyconsolas.com/listas/20-videojuegos-mas-populares-historia-357957"
},

{
    "title": "Conoce todo sobre los E-Sports",
    "url_image":"https://img.blogs.es/anexom/wp-content/uploads/2018/10/esports2-920x515.jpg",
    "desc":"Te comentamos que son los E-Sports y todas sus caracteristicas",
    "cta":"Show More",
    "link":"https://es.wikipedia.org/wiki/Deportes_electr%C3%B3nicos"
},

{
    "title": "Conoce el Mundo Mario",
    "url_image":"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQQbZR2hOPNPhOUewvWzWOhVEAmvo3urjaqmA&usqp=CAU",
    "desc":"Conozcamos un poco mas sobre Mario Bross",
    "cta":"Show More",
    "link":"https://mario.fandom.com/es/wiki/Mario"
}];

(function () {
    let CARD = {
        init: function () {
            //consol.log('card module was loaded');
            let _self = this;

            //llamamoslas funciones
            this.insertData(_self);
            //this.eventHandler(_self);
        },

        eventHandler: function (_self){
            let arrayRefs = document.querySelectorAll('.accordion-title');

            for (let x = 0; x < arrayRefs.length; x++){
                arrayRefs[x].addEventListener('click', function(event){
                    console.log('event', event);
                    _self.showTab(event.target);
                });
            }
        },

        insertData: function (_self){
            dataCards.map(function (item, index){
                document.querySelector('.card-list').insertAdjacentHTML('beforeend', _self.tplCardItem(item, index));
            });
        },

        tplCardItem: function (item, index){
            return(`<div class='card-item' id="card-number-${index}">
            <img src="${item.url_image}"/>
            <div class='card-info'>
            <p class='card-title'>${item.title}</p>
            <p class='card-desc'>${item.desc}</p>
            <a class='card-cta' target="blank" href="${item.link}">${item.cta}</a>
            </div>
            </div>`)},
        }
       CARD.init();

})();